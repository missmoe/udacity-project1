<html <?php language_attributes(); ?>>
  <head>
  	<title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />

   <?php wp_head(); ?>

  </head>
  <body>